using System;
using Cercle_class;
using Point_class;

namespace Cercle_class
{


    class Main_program
    {


        internal static void Program()

        {
            Console.WriteLine("");
             Console.WriteLine(" CERCLE ");
            Console.Write(" Entrer l'abscisse du centre : ");
            double x = double.Parse(Console.ReadLine());
            Console.Write(" Enter l'ordonne du centre : ");
            double y = double.Parse(Console.ReadLine());
            Console.Write(" Entrer le rayon du Cercle: ");
            double r = double.Parse(Console.ReadLine());

            //creation cercle
            Console.WriteLine("CERCLE");
            Cercle cercle = new Cercle(x, y, r);
            cercle.Display();
            cercle.Perimetre();
            cercle.Surface();

            //input point a verifier
            Console.WriteLine("Entrer  un point a verifier ");
            Console.Write("  Abscice X : ");
            int monx = int.Parse(Console.ReadLine() ?? "");
            Console.Write("   Ordonnee Y : ");
            int mony = int.Parse(Console.ReadLine() ?? "");

            //creation d'un point
            Point point = new Point(monx, mony);
            point.View();

            //verification de l'appartenance du point 
            point.VerificationPoint(monx, mony, x, y, r);

        }
    }
}
