using System;
using System.Security.Cryptography;
using Alarm;
using ClockApp;

namespace Horloge
{

    class Main_program
    {
        internal static void Program()
        {
            int choice;
             Console.WriteLine("");
             Console.WriteLine("HORLOGE");

            do
            {
                DisplayMenu();
                choice = GetChoice();
                Console.Clear();

                switch (choice)
                {
                    case 1:
                        AlarmApp alarmApp = new AlarmApp();
                        alarmApp.Run();
                        break;
                    case 2:
                        ClockApp.ClockApp.RunClockApp();
                        break;

                    default:
                        break;
                }
            } while (choice != 3);

            void DisplayMenu()

            {
           
            Console.WriteLine("");
            Console.WriteLine("MENU\n1) Alarme\n2) Horloge\n3) Quitter ");
            Console.Write("Entrer Votre choix : ");
            
            }
            
            int GetChoice()
            {
                int choice;
                while (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("saisie erroner");
                    Console.Write("Votre choix : ");
                }
                return choice;
            }



        }
    }
}