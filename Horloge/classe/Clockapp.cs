using System;
using System.Collections.Generic;


namespace ClockApp
{
    class ClockApp
    {
        public static void RunClockApp(){
            ClockApp clockApp=new ClockApp();
            clockApp.Run();
        }
        private List<Clock> clockList = new List<Clock>();

        public void Run()
        {
            int choice;

            do
            {
                DisplaySubMenu();
                choice = GetSubMenuChoice();
                Console.Clear();

                switch (choice)
                {
                    case 1:
                        ViewActiveClocks();
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    case 2:
                        AddClock();
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    case 3:
                        Console.WriteLine("Retour au menu principal");
                         Console.ReadKey();
                            Console.Clear(); 
                        break;
                    default:
                        Console.WriteLine("Choix non valide. Veuillez réessayer ");
                        break;
                }

            } while (choice != 3);
        }

        private void DisplaySubMenu()

        {
            Console.WriteLine("""Choisisssez\n*************** 1) Voir les Horloges actives ************\n*************** 2) Ajouter une horloge ******************\n*************** 3) Retour au menu principale ************""");
            Console.Write("Choix :");
        
        }

        private int GetSubMenuChoice()
        {
            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice))
            {
                Console.WriteLine(" Entrée non valide. Veuillez entrer un nombre ");
                Console.Write(" Choix :");
               
            }
            return choice;
        }

        private void ViewActiveClocks()
        {
            
            Console.WriteLine("                 Liste des horloges :");
            DisplayCurrentTime();
            foreach (var clock in clockList)
            {
                Console.WriteLine(clock.ToString());
            }

            
             
        }

        private void DisplayCurrentTime()
        {
            // Afficher le temps actuel (peut être mis à jour en temps réel)
            Console.WriteLine($"""
            Antananarivo :
            {DateTime.Now.ToString("HH:mm:ss")}
            {DateTime.Now.ToString("ddd dd MMM")}
            """);
            
        }

        private void AddClock()
        {
            Console.WriteLine("              Choisissez une ville ");
            Console.Write("[Dubaï,Japon,Argentine,Canada] ");
            string city = Console.ReadLine() ?? "";

            Clock newClock = new Clock(city);
            clockList.Add(newClock);

            Console.WriteLine("                Votre horloge a bien été enregistrée )");
            
        }
    }

    class Clock
    {
        public string City { get; set; }
        public int TimeDifference { get; set; } // Différence par rapport à l'heure locale

        public Clock(string city)
        {
            City = city;
            // Vous pouvez ajuster les différences de temps en fonction du fuseau horaire de chaque ville
            TimeDifference = GetTimeDifference(city);
        }

        private static int GetTimeDifference(string city)
        {
            return city.ToLower() switch
            {
                "dubaï" => 4,
                "japon" =>9,
                "Argentine" =>-3,
                "Canada" =>-7,
                _ => 0,/*Par défaut*/
            };
        }

        public override string ToString()

        {
            DateTime localTime=DateTime.UtcNow.AddHours(TimeDifference);
            string timeString= localTime.ToString("HH:mm");
            return $"""
            Autre horloge 
            {City} - {timeString} (GMT{(TimeDifference >= 0 ? "+" : "")}{TimeDifference} / {localTime}) 
            """;
        }
    }

}