using System;
using System.Collections.Generic;
namespace Alarm
{



    class AlarmApp
    {
            private List<Alarm> alarmList = new List<Alarm>();

            public void Run()
            {
                int choice;

                do
                {
                    DisplaySubMenu();
                    choice = GetSubMenuChoice();
                    Console.Clear();

                    switch (choice)
                    {
                        case 1:
                            ViewActiveAlarms();
                            Console.ReadKey();
                            Console.Clear();                            break;
                        case 2:
                            CreateAlarm();
                             Console.ReadKey();
                            Console.Clear(); 
                            break;
                        case 3:
                            Console.WriteLine("  Retour au menu principal");
                            Console.ReadKey();
                            Console.Clear(); 
                            break;
                        default:
                            Console.WriteLine("Choix non valide. Veuillez réessayer");
                            break;
                    }

                } while (choice != 3);
            }

            private void DisplaySubMenu()
            {
                Console.WriteLine("""
                               Choisissez une option 
                ############ 1) Voir les alarmes actives ##############
                ############ 2) Créer une alarmes #####################
                ############ 3) Quitter ###############################
                """);
                Console.Write(" choix ");
               
            }

            private int GetSubMenuChoice()
            {
                int choice;
                while (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Entrée non valide. Veuillez entrer un nombre ");
                    Console.Write(" choix ");
                    Console.Clear();
                }
                return choice;
            }

            private void ViewActiveAlarms()
            {
                Console.WriteLine("            Liste des alarmes actives ");
                foreach (var alarm in alarmList)
                {
                    Console.WriteLine($" {alarm.ToString()} ");
                   

                }
            }

            private void CreateAlarm()
            {
                Console.WriteLine("             Info de la nouvelle alarme ");

                Console.Write(" Reference : ");
                string name = Console.ReadLine();

                Console.Write(" Heure  (hh:mm) : ");
                string time = Console.ReadLine();

                Console.Write(" Date de planification (L/Ma/Me/J/V/S/D) : ");
                string jour = Console.ReadLine().ToUpper();

                Console.Write(" Lancer une seule fois (y/n) : ");
                bool isSingleUse = Console.ReadLine().ToLower() == "y";

                Console.Write(" Périodique ?  (y/n) : ");
                bool isPeriodic = Console.ReadLine().ToLower() == "y";

                Console.Write(" Activer la sonnerie par défaut (y/n) : ");
                bool useDefaultRingtone = Console.ReadLine().ToLower() == "y";


            jour = jour switch
            {
                "L" => "Lundi",
                "Ma" => "Mardi",
                "Me" => "Mercredi",
                "J" => "Jeudi",
                "V" => "Vendredi",
                "S" => "Samedi",
                "D" => "Dimanche",
                _ => "Lundi-Mardi-Mercredi-Jeudi-Vendredi-Samedi-Dimache",
            };
            string scheduledTime = $"{jour}:{time}";


                Alarm newAlarm = new Alarm(name, scheduledTime, isSingleUse, isPeriodic, useDefaultRingtone);
                alarmList.Add(newAlarm);

                Console.WriteLine("Votre nouvelle alarme est enregistrée :)");
                
            }




        }

        class Alarm
        {
       

        public string Name { get; set; }
            public  string ScheduledTime { get; set; }
            public bool IsSingleUse { get; set; }
            public bool IsPeriodic { get; set; }
            public bool UseDefaultRingtone { get; set; }

            public Alarm(string name, string scheduledTime, bool isSingleUse, bool isPeriodic, bool useDefaultRingtone)
            {
                Name = name;
                ScheduledTime = scheduledTime;
                IsSingleUse = isSingleUse;
                IsPeriodic = isPeriodic;
                UseDefaultRingtone = useDefaultRingtone;
            }

      

        public override string ToString()
            {
                string alarmInfo = $"""
                {Name} ---- {ScheduledTime:hh:mm}
             
                """;
                if (IsPeriodic)
                {
                    alarmInfo += $" (périodique) ";
                }
                return alarmInfo;
            }





        

    }
}