using System;
using Client_class;
using Compte_class;
namespace Compte_banque
{


    class Main_program
    {


        internal static void Program()
        {
#pragma warning disable CS8632
            string? cin, firstName, lastName, tel;
#pragma warning restore CS8632
            float? somme;
            Console.WriteLine(" COMPTE BANQUE ");
            Console.WriteLine("COMPTE 1");
            Console.Write($"Donner Le CIN: ");
            cin = Console.ReadLine();
            Console.Write($"Donner Le Nom: ");
            firstName = Console.ReadLine();
            Console.Write($"Donner Le Prenom: ");
            lastName = Console.ReadLine();
            Console.Write($"Donner Le numero de telephone: ");
            tel = Console.ReadLine();

            Compte cpt1 = new Compte(new Client(cin, firstName, lastName, tel));
            Console.WriteLine($"Details du compte:{cpt1.Resumer()}");


            // Crediter & Debiter Compte Client 1
            Console.Write($"Donner le montant a deposer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Crediter((float)somme);
            Console.WriteLine($" == Operation bien effectuee{cpt1.Resumer()} ==");
            //Debiter cpt 1
            Console.Write($"Donner le montant a retirer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Debiter((float)somme);
            Console.Write($" Operation bien effectuee{cpt1.Resumer()} ");
            
            Console.ReadKey();
            // # Create Compte Client 2
            Console.WriteLine("COMPTE 2");
            Console.Write($"\n\n\nCompte 2:\nDonner Le CIN: ");
            cin = Console.ReadLine();
            Console.Write($"Donner Le Nom: ");
            firstName = Console.ReadLine();
            Console.Write($"Donner Le Prenom: ");
            lastName = Console.ReadLine();
            Console.Write($"Donner Le numero de telephone: ");
            tel = Console.ReadLine();

            Compte cpt2 = new Compte(new Client(cin, firstName, lastName, tel));
            Console.WriteLine($"Details du compte:{cpt2.Resumer()}");


            Console.ReadKey();
            Console.WriteLine("***********************************");
            //Crediter cpt2 -> cpt1
            Console.WriteLine($" Crediter le compte {cpt2.Code} a partir du compte ==> {cpt1.Code} ");
            Console.Write($"Donner le montant a deposer: ");
            somme = float.Parse(Console.ReadLine());
            cpt2.Crediter((float)somme, cpt1);
            Console.WriteLine($" Operation bien effectuee ");
             Console.WriteLine("***********************************");
            
            
            //Debiter cpt1 -> cpt2
            Console.WriteLine($"Debiter le compte {cpt1.Code} et crediter le compte {cpt2.Code}");
            Console.Write($"Donner le montant a retirer: ");
            somme = float.Parse(Console.ReadLine());
            cpt1.Debiter((float)somme, cpt2);
            Console.WriteLine($" Operation bien effectuee  ");

             
              Console.WriteLine("**********************************");
               Console.WriteLine("###########################");
            Console.WriteLine($"{cpt1.Resumer()}{cpt2.Resumer()}");
            Console.Write($"\n\n\n{Compte.NombreCompteCree()}");
            Console.ReadKey();
        }
    }
}
